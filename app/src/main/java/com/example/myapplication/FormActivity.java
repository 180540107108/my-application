package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.database.MyDatabase;
import com.example.myapplication.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class FormActivity extends AppCompatActivity {

    EditText etName, etPhoneNumber, etEmail;
    ImageView ivClose;
    TextView tvDisplay;
    Button btnSubmit;
    ImageView ivBackground;

    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFemale;

    CheckBox chbCricket;
    CheckBox chbFootBall;
    CheckBox chbHockey;

    Button btnName;
    Button btnPhoneNumber;
    Button btnEmail;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("TextViewValue", tvDisplay.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_activity);
        initViewReference();
        intViewEvent();
        /*setTypefaceOnView();*/
        handleSavedInstance(savedInstanceState);
    }

    void handleSavedInstance(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            tvDisplay.setText(savedInstanceState.getString("TextViewValue"));
        }
    }


    void intViewEvent() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {


                    etName.requestFocus();
                    etPhoneNumber.requestFocus();
                    etEmail.requestFocus();


                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Const.NAME, etName.getText().toString());
                    map.put(Const.EMAIL_ADDRESS, etEmail.getText().toString());
                    map.put(Const.GENDER, rbMale.isChecked() ? "M" : "F");
                    String hobbies = "";
                    if (chbCricket.isChecked()) {
                        hobbies += "," + chbCricket.getText().toString();
                    }
                    if (chbFootBall.isChecked()) {
                        hobbies += "," + chbFootBall.getText().toString();
                    }
                    if (chbHockey.isChecked()) {
                        hobbies += "," + chbHockey.getText().toString();
                    }
                    map.put(Const.HOBBY, hobbies);
                    userList.add(map);

                    Intent intent = new Intent(FormActivity.this, DisplayActivity.class);
                    intent.putExtra("UserList", userList);
                    startActivity(intent);

                    etName.setText("");
                    etEmail.setText("");
                    etPhoneNumber.setText("");
                }
            }
        });

        tvDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String abc = "some one has $192 for currency conversion @70";
                int intValueGetFromEditText;
                if (etPhoneNumber.getText().toString().length() > 0) {
                    try {
                        intValueGetFromEditText = Integer.parseInt(etPhoneNumber.getText().toString());
                    } catch (Exception e) {
                        intValueGetFromEditText = 0;
                    }
                }
            }
        });
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rbActMale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                } else if (i == R.id.rbActFemale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.GONE);
                }
            }
        });

        btnName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etName.setText("");
            }
        });

        btnPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPhoneNumber.setText("");
            }
        });

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etEmail.setText("");
            }
        });
    }

    void initViewReference() {
        new MyDatabase(FormActivity.this);

        etName = findViewById(R.id.etActName);
        etPhoneNumber = findViewById(R.id.etActPhoneNumber);
        etEmail = findViewById(R.id.etActEmail);
        tvDisplay = findViewById(R.id.tvActDisplay);
        btnSubmit = findViewById(R.id.btnActSubmit);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbFootBall = findViewById(R.id.chbActFootBall);
        chbCricket = findViewById(R.id.chbActCricket);
        chbHockey = findViewById(R.id.chbActHockey);

        btnName = findViewById(R.id.btnActName);
        btnPhoneNumber = findViewById((R.id.btnActPhoneNumber));
        btnEmail = findViewById(R.id.btnActEmail);

        getSupportActionBar().setTitle(R.string.lbl_form_activity);
        /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.home_screen_menu, menu);
        return true;
    }


    boolean isValid() {
        boolean flag = true;

        //Email Address Validation
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.error_enter_value));
            flag = false;
            etEmail.requestFocus();
        } else {
            String email = etEmail.getText().toString();
            String emailPattern = "([a-zA-Z0-9._-]+)@([a-z]+)\\.([a-z]{2,8})(\\.[a-z]{2,8})?";
            if (!email.matches(emailPattern)) {
                etEmail.setError("Enter Valid Email Address");
                flag = false;
                etEmail.requestFocus();
            }
        }

        //Phone Number Validation
        if (TextUtils.isEmpty(etPhoneNumber.getText())) {
            etPhoneNumber.setError(getString(R.string.error_enter_value));
            flag = false;
            etPhoneNumber.requestFocus();
        } else {
            String phoneNumber = etPhoneNumber.getText().toString();
            if (phoneNumber.length() < 10) {
                etPhoneNumber.setError("Enter Valid Phone Number");
                flag = false;
                etPhoneNumber.requestFocus();
            }
        }


        //Name Validation
        if (TextUtils.isEmpty(etName.getText().toString().trim())) {
            etName.setError(getString(R.string.error_enter_value));
            flag = false;
            etName.requestFocus();
        } else {
            String name = etName.getText().toString();
            String namePattern = "([a-zA-Z ]+)";
            if (!name.matches(namePattern)) {
                etEmail.setError("Enter Valid Name");
                flag = false;
                etName.requestFocus();
            }
        }


        //CheckBox Validation
        if (!(chbCricket.isChecked() || chbFootBall.isChecked() || chbHockey.isChecked())) {
            Toast.makeText(this, "Please Select any one Checkbox", Toast.LENGTH_LONG).show();
            flag = false;
        }

        return flag;
    }
}